package rockingBoat.vaultClient.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class ResponseTokenLookup(
    val data: ResponseTokenLookupData
) : ResponseBase()

