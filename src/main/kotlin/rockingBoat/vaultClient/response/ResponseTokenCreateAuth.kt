package rockingBoat.vaultClient.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class ResponseTokenCreateAuth(
    @field:JsonProperty("client_token")
    val token: String,

    val policies: List<String>?,

    @field:JsonProperty("metadata")
    val meta: Map<String, String>?,

    @field:JsonProperty("lease_duration")
    val leaseDuration: Int,

    val renewable: Boolean

) : ResponseBase()