package rockingBoat.vaultClient.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class ResponseTokenCreate(
    val auth: ResponseTokenCreateAuth
) : ResponseBase()

