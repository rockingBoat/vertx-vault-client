package rockingBoat.vaultClient.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class ResponseKVGet(
    @field:JsonProperty("data")
    val data: Any?,

    @field:JsonProperty("lease_duration")
    var leaseDuration: Int = 0,

    @field:JsonProperty("lease_id")
    var leaseId: Int = 0,

    @field:JsonProperty("renewable")
    val renewable: Boolean
) : ResponseBase()
