package rockingBoat.vaultClient.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.Date

@JsonIgnoreProperties(ignoreUnknown = true)
data class ResponseTokenLookupData(
    val id: String,

    @field: JsonProperty("accessor")
    val accessor: String,

    @field: JsonProperty("creation_time")
    val creationTime: Int,

    @field: JsonProperty("creation_ttl")
    val creationTTL: Int,

    @field: JsonProperty("display_name")
    val displayName: String,

    @field: JsonProperty("entity_id")
    val entityId: String,

    @field: JsonProperty("expire_time")
    val expireTime: Date?,

    @field: JsonProperty("explicit_max_ttl")
    val explicitMaxTTL: Int,

    @field: JsonProperty("issue_time")
    val issueTime: Date,

    val meta: Map<String, String>?,

    @field: JsonProperty("num_uses")
    val numUses: Int,

    val orphan: Boolean,

    val path: String,

    val policies: List<String>,

    val renewable: Boolean,

    val ttl: Int

) : ResponseBase()