package rockingBoat.vaultClient.request

import com.fasterxml.jackson.annotation.JsonProperty

data class RequestTokenCreateRole(

    @field: JsonProperty("role_name")
    val roleName: String,

    @field: JsonProperty("allowed_policies")
    var allowedPolicies: List<String>? = null,

    @field: JsonProperty("disallowed_policies")
    var disallowedPolicies: List<String>? = null,

    var orphan: Boolean? = null,
    var period: String? = null,
    var renewable: Boolean? = null,

    @field: JsonProperty("explicit_max_ttl")
    var explicitMaxTTL: Int? = null,

    @field: JsonProperty("path_suffix")
    var pathSuffix: String? = null,

    @field: JsonProperty("bound_cidrs")
    var boundCIDRS: List<String>? = null
)