package rockingBoat.vaultClient.request

import com.fasterxml.jackson.annotation.JsonProperty

data class RequestTokenCreate(

    var id: String? = null,

    @field: JsonProperty("role_name")
    var roleName: String? = null,

    var policies: List<String>? = null,
    var meta: Map<String, String>? = null,

    @field: JsonProperty("no_parent")
    var noParent: Boolean? = null,

    @field: JsonProperty("no_default_policy")
    var noDefaultPolicy: Boolean? = null,

    var renewable: Boolean? = null,
    var ttl: String? = null,

    @field: JsonProperty("explicit_max_ttl")
    var explicitMaxTTL: String? = null,

    @field: JsonProperty("display_name")
    var displayName: String? = null,

    @field: JsonProperty("num_uses")
    var num_uses: Int? = null,

    var period: String? = null
)

