package rockingBoat.vaultClient

data class VaultClientOptions(
    var token: String,
    var host: String = "http://localhost",
    var port: Int = 8200
)