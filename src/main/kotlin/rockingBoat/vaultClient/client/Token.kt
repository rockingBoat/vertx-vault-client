package rockingBoat.vaultClient.client

import io.vertx.core.http.HttpMethod
import rockingBoat.vaultClient.VaultClient
import rockingBoat.vaultClient.request.RequestTokenCreate
import rockingBoat.vaultClient.request.RequestTokenCreateRole
import rockingBoat.vaultClient.response.ResponseBase
import rockingBoat.vaultClient.response.ResponseKVGet
import rockingBoat.vaultClient.response.ResponseTokenCreate
import rockingBoat.vaultClient.response.ResponseTokenLookup

suspend fun VaultClient.tokenLookup(token: String) = realRequest<ResponseTokenLookup>(
    method = HttpMethod.POST,
    path = "/auth/token/lookup",
    data = mapOf("token" to token)
)

suspend fun VaultClient.tokenLookupAccessor(accessor: String) = realRequest<ResponseTokenLookup>(
    method = HttpMethod.POST,
    path = "/auth/token/lookup-accessor",
    data = mapOf("accessor" to accessor)
)

suspend fun VaultClient.tokenCreateOrphan(request: RequestTokenCreate) = realRequest<ResponseTokenCreate>(
    method = HttpMethod.POST,
    path = "/auth/token/create-orphan",
    data = request
)

suspend fun VaultClient.tokenCreate(request: RequestTokenCreate, role: String? = null) = realRequest<ResponseTokenCreate>(
    method = HttpMethod.POST,
    path = "/auth/token/create/${role ?: ""}",
    data = request
)

suspend fun VaultClient.tokenLookupSelf(token: String? = null) = realRequest<ResponseTokenLookup>(
    method = HttpMethod.GET,
    path = "/auth/token/lookup-self",
    anotherToken = token
)

suspend fun VaultClient.tokenCreateRole(request: RequestTokenCreateRole) = realRequest<ResponseBase>(
    method = HttpMethod.POST,
    path = "/auth/token/roles/${request.roleName}",
    data = request
)

suspend fun VaultClient.tokenRevoke(token: String) = realRequest<ResponseBase>(
    method = HttpMethod.POST,
    path = "/auth/token/revoke",
    data = mapOf("token" to token)
)

suspend fun VaultClient.tokenRevokeSelf(token: String? = null) = realRequest<ResponseBase>(
    method = HttpMethod.POST,
    path = "/auth/token/revoke",
    anotherToken = token
)

suspend fun VaultClient.tokenRenew(token: String, increment: String? = null) = realRequest<ResponseTokenCreate>(
    method = HttpMethod.POST,
    path = "/auth/token/renew",
    data = mutableMapOf("token" to token).apply { increment?.let { this@apply.set("increment", it) } }
)

suspend fun VaultClient.tokenRenewSelf(token: String? = null, increment: String? = null) = realRequest<ResponseTokenCreate>(
    method = HttpMethod.POST,
    path = "/auth/token/renew-self",
    data = increment?.let { mapOf("increment" to increment) },
    anotherToken = token
)

suspend fun VaultClient.tokenRevokeAccessor(accessor: String) = realRequest<ResponseBase>(
    method = HttpMethod.POST,
    path = "/auth/token/revoke-accessor",
    data = mapOf("accessor" to accessor)
)

suspend fun VaultClient.tokenRevokeOrphan(token: String) = realRequest<ResponseBase>(
    method = HttpMethod.POST,
    path = "/auth/token/revoke-orphan",
    data = mapOf("token" to token)
)

suspend fun VaultClient.tokenDeleteRole(role: String) = realRequest<ResponseBase>(
    method = HttpMethod.DELETE,
    path = "/auth/token/roles/$role"
)

suspend fun VaultClient.tokenTidy() = realRequest<ResponseBase>(
    method = HttpMethod.POST,
    path = "/auth/token/tidy"
)

// TODO: Wait for 3.6.0 vert.x - https://github.com/vert-x3/vertx-web/issues/845, currently we can set method like LIST

// TODO: Make "List Accessors"
// TODO: Make "Read Token Role"
// TODO: Make "List Token Roles"
