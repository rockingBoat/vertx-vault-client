package rockingBoat.vaultClient.client

import io.vertx.core.http.HttpMethod
import rockingBoat.vaultClient.VaultClient
import rockingBoat.vaultClient.response.ResponseBase
import rockingBoat.vaultClient.response.ResponseKVGet

// KV V1

suspend fun VaultClient.kvUpdate(path: String, data: Any) = realRequest<ResponseBase>(
    method = HttpMethod.PUT,
    path = "/secret/data$path",
    data = data
)

suspend fun VaultClient.kvCreate(path: String, data: Any) = realRequest<ResponseBase>(
    method = HttpMethod.POST,
    path = "/secret/data$path",
    data = data
)

suspend fun VaultClient.kvDelete(path: String) = realRequest<ResponseBase>(
    method = HttpMethod.DELETE,
    path = "/secret/data$path"
)

suspend fun VaultClient.kvGet(path: String) = realRequest<ResponseKVGet>(
    method = HttpMethod.GET,
    path = "/secret/data$path"
)
