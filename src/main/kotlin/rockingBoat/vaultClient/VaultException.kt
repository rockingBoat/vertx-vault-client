package rockingBoat.vaultClient

class VaultException(message: String, val code: Int, val body: String?) : Exception(message) {

    companion object {
        fun toFailure(status: String, code: Int, body: String?): Exception {
            val message = StringBuilder()
            message.append("Vault responded with HTTP status: ").append(status)
            if (body != null && !body.isEmpty()) {
                message.append("\nresponse body:").append(body)
            }
            return VaultException(message.toString(), code, body)
        }
    }
}
