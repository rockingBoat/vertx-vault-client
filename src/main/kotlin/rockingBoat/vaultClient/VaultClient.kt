package rockingBoat.vaultClient

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import io.vertx.core.AsyncResult
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import rockingBoat.vaultClient.response.ResponseBase
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.reflect.full.createInstance

class VaultClient(private val vertx: Vertx, val options: VaultClientOptions) {
    val client: WebClient by lazy {

        WebClient.create(
            vertx,
            WebClientOptions().apply {
                defaultHost = options.host
                defaultPort = options.port
                logActivity = true
                isFollowRedirects = true
            }
        )
    }

    val mapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule())
    val tokenHeader = "X-Vault-Token"

    @SuppressWarnings("unused")
    fun close() = client.close()

    suspend inline fun <reified T : ResponseBase> realRequest(method: HttpMethod,
        path: String,
        data: Any? = null,
        anotherToken: String? = null) = suspendCoroutine<T> { cont ->

        val r = client.request(method, "/v1$path").putHeader(tokenHeader, anotherToken ?: options.token)

        val handler = { response: AsyncResult<HttpResponse<Buffer>?> ->

            val result = response.result()

            if (result != null && response.succeeded()) {
                when (result.statusCode()) {
                    200 -> cont.resume(mapper.readValue(result.bodyAsString()))
                    in 201..204 -> cont.resume(T::class.createInstance())
                    else -> cont.resumeWithException(VaultException.toFailure(
                        result.statusMessage(),
                        result.statusCode(),
                        result.bodyAsString()
                    ))
                }
            } else {
                cont.resumeWithException(response.cause())
            }
        }


        when (method) {
            HttpMethod.GET,
            HttpMethod.DELETE -> r.send(handler)

            HttpMethod.POST,
            HttpMethod.PUT -> r.sendJson(data ?: emptyMap<String, String>(), handler)

            else -> TODO()
        }
    }
}

