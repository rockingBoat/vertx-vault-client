import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.gradle.dsl.Coroutines



plugins {
    kotlin("jvm") version "1.3.10"
    `maven`
}


group = "rockingBoat"
version = "0.0.2"



buildscript {
    val kotlinVersion by extra { "1.3.10" }

    repositories {
        mavenCentral()
        jcenter()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
    }
}

repositories {
    mavenCentral()
    maven { setUrl("https://jitpack.io") }
}

dependencies {
    val vertxVersion by extra { "3.6.0.CR1" }
    val jacksonVersion by extra { "2.9.6" }
    val kotlinxCoroutinesVersion by extra { "1.0.1" }

    compile(kotlin("stdlib-jdk8"))
    compile(kotlin("reflect"))
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinxCoroutinesVersion")

    compile("io.vertx:vertx-core:$vertxVersion")
    compile("io.vertx:vertx-lang-kotlin-coroutines:$vertxVersion")
    compile("io.vertx:vertx-web:$vertxVersion")
    compile("io.vertx:vertx-web-client:$vertxVersion")

    compile("com.fasterxml.jackson.core:jackson-core:$jacksonVersion")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion")
//    compile("com.gitlab.rockingBoat:vertx-extension:428cf4b3ee")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
